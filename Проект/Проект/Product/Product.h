#pragma once
#include "../String/String.h"
#include <iostream>

struct Product {
	unsigned long int _id;
	String _title;
	String _desc;
	String _createDate;
	double _price;
	Product();
	Product(Product& product);
	Product(unsigned long int id, const char* title, const char* desc, const char* createDate, double price);
};

std::ostream& operator<< (std::ostream& out, const Product& product);