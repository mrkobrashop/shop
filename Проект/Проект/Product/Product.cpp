#include "Product.h"

Product::Product() : _id(0), _title(String()), _desc(String()), _createDate(), _price(0) {}

Product::Product(Product& product) : _id(product._id), _title(product._title), _desc(product._desc),
	_createDate(product._createDate), _price(product._price) {}

Product::Product(unsigned long int id, const char* title, const char* desc, const char* createDate, double price) :
	_id(id), _title(String(title)), _desc(String(_desc)), _createDate(String(createDate)), _price(price) {}

std::ostream& operator<< (std::ostream& out, const Product& product)
{
	out << product._title;
	return out;
}