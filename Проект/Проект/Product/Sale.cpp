#include "Sale.h"
#include "../Base/DataBase.h"
#include "../Global/Global.h"

Sale::Sale() : _id(0), _totalPrice(0), _productId(0), _createDate(String()), _count(0), _userId(0) {}

Sale::Sale(Sale& sale) : _id(sale._id), _totalPrice(sale._totalPrice), _productId(sale._productId),
	_createDate(sale._createDate), _count(sale._count), _userId(sale._userId) {}

Sale::Sale(unsigned long int id, double totalPrice, unsigned long int productId, const char* createDate, int count, unsigned long int userId) :
	_id(id), _totalPrice(totalPrice), _productId(productId), _createDate(String(createDate)),
	_count(count), _userId(userId) {}

std::ostream& operator<< (std::ostream& out, const Sale& sale)
{
	Product* product = nullptr;
	if (GetListItem(productHead, String("ID"), sale._productId)) {
		product = GetListItem(productHead, String("ID"), sale._productId)->_item;
		if (product != nullptr) {
			out << *product;
		}
	}
	return out;
}