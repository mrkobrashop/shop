#pragma once
#include "Product.h"
#include "../String/String.h"
#include <iostream>

struct Sale {
	unsigned long int _id;
	double _totalPrice;
	unsigned long int _productId;
	String _createDate;
	int _count;
	unsigned long int _userId;
	Sale();
	Sale(Sale& sale);
	Sale(unsigned long int id, double totalPrice, unsigned long int productId, const char* createDate, int count, unsigned long int userId);
};

std::ostream& operator<< (std::ostream& out, const Sale& sale);
