#pragma once
#include <iostream>
#include "../String/String.h"
class User {
public:
	User();
	User(const User& user);
	User(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate);
	String GetHumanMeta(const char* field);
	unsigned long int GetId();
	friend std::ostream& operator << (std::ostream& out, const User& user);
	friend std::istream& operator>> (std::istream& in, User& user);
	void Set(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate);
	void SetId(unsigned long int id);
	void SetMeta(const char* meta, const char* field);
protected:
	unsigned long int _id;
	String _username;
	String _password;
	String _name;
	String _createDate;
};
