#pragma once
#include "User.h"

class Client : public User {
public:
	Client();
	Client(Client& client);
	Client(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate);
};