#include "Employee.h"

Employee::Employee() : User() {}

Employee::Employee(Employee& employee) : User(employee) {}

Employee::Employee(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate) :
	User(id, username, password, name, createDate) {}