#include "User.h"
#include "../Base/DataBase.h"
#include "../Global/Global.h"

User::User() : _id(0), _username(String()), _password(String()), _name(String()), _createDate(String()) {}

User::User(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate) : 
	_id(id), _username(String(username)), _password(String(password)), _name(String(name)), _createDate(String(createDate)) {}

User::User(const User& user) : _id(user._id), _username(user._username), _password(user._password),
	_name(user._name), _createDate(user._createDate) {}

String User::GetHumanMeta(const char* field)
{
	String result{};
	if (String(field) == "Username") {
		result = _username;
	}
	else if (String(field) == "Password") {
		result = _password;
	}
	else if (String(field) == "Name") {
		result = _name;
	}
	else if (String(field) == "Create date") {
		result = _createDate;
	}
	return result;
}
unsigned long int User::GetId()
{
	return _id;
}
std::ostream& operator << (std::ostream& out, const User& user)
{
	out << user._name;
	return out;
}

std::istream& operator>> (std::istream& in, User& user)
{
	String username{}, password{}, name{}, createDate{};
	std::cout << "Input username: ";
	in >> username;
	std::cout << "Input password: ";
	in >> password;
	std::cout << "Input name: ";
	in >> name;
	std::cout << "Input create date: ";
	in >> createDate;
	user._id = 1;
	user._username = username;
	user._password = password;
	user._name = name;
	user._createDate = createDate;
	return in;
}

void User::Set(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate)
{
	_id = id;
	_username = String(username);
	_password = String(password);
	_name = String(name);
	_createDate = String(createDate);
}
void User::SetId(unsigned long int id)
{
	_id = id;
}
void User::SetMeta(const char* meta, const char* field)
{
	if (String(field) == "Username") {
		_username = String(meta);
	}
	else if (String(field) == "Password") {
		_password = String(meta);
	}
	else if (String(field) == "Name") {
		_name = String(meta);
	}
	else if (String(field) == "Create date") {
		_createDate = String(meta);
	}
}