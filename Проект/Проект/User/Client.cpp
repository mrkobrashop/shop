#include "Client.h"

Client::Client() : User() {}

Client::Client(Client& client) : User(client) {}

Client::Client(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate) :
	User(id, username, password, name, createDate) {}