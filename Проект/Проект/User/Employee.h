#pragma once
#include "User.h"
class Employee : public User {
	Employee();
	Employee(Employee& employee);
	Employee(unsigned long int id, const char* username, const char* password, const char* name, const char* createDate);
};