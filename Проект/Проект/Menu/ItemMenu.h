#pragma once
#include "../String/String.h"
#include "../Screens/InterfaceScreen.h"
#include <iostream>

class ItemMenu {
	typedef int (*Func)();
public:
	ItemMenu(const char* itemName, Func func);
	ItemMenu();
	ItemMenu(const ItemMenu& item);
	Func GetFunc();
	String GetItemName();
	void Set(String itemName, Func func);
	int Run();
	friend std::ostream& operator << (std::ostream& out, ItemMenu& item);
private:
	String _itemName;
	Func _func;
};
