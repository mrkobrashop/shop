#include "ItemMenu.h"
#include "../Global/Global.h"

ItemMenu::ItemMenu(const char* itemName, Func func) : _itemName(itemName), _func(func) {}

ItemMenu::ItemMenu()
{
	_itemName = String();
	_func = nullptr;
}

ItemMenu::ItemMenu(const ItemMenu& item) : _itemName(item._itemName), _func(item._func) {}

ItemMenu::Func ItemMenu::GetFunc()
{
	return _func;
}

String ItemMenu::GetItemName()
{
	return _itemName;
}

void ItemMenu::Set(String itemName, Func func)
{
	_itemName = itemName;
	_func = func;
}

int ItemMenu::Run()
{
	int result{};
	if (_func != nullptr) {
		result = (_func)();
	}
	else {
		result = 1;
	}
	return result;
}
std::ostream& operator << (std::ostream& out, ItemMenu& item)
{
	return out << item._itemName;
}
