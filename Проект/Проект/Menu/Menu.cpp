#include "Menu.h"
Menu::Menu(const char* title, size_t count, ItemMenu* items) : _select(-1), _running(false), _title(title), _count(count), _items(items) {}

Menu::Menu() : _select(-1), _running(false), _title(nullptr), _count(0), _items(nullptr) {}

Menu::Menu(const Menu& menu) : _select(-1), _running(false), _title(menu._title), _count(menu._count), _items(menu._items) {}

int Menu::GetSelect()
{
	return _select;
}
bool Menu::GetRunning()
{
	return _running;
}
String Menu::GetTitle()
{
	return _title;
}
size_t Menu::GetCount()
{
	return _count;
}
ItemMenu* Menu::GetItems()
{
	return _items;
}
std::ostream& operator << (std::ostream& out, Menu& menu)
{
	out << menu._title << std::endl;
	for (size_t i = 0; i < menu._count; i++)
	{
		out << i + 1 << ". " << menu._items[i] << std::endl;
	}
	out << menu._count + 1 << ". Exit";
	return out;
}
void Menu::ProcessMenu() {
	this->_running = true;
	do {
		system("cls");
		std::cout << *this << std::endl;
		std::cout << "- > ";
		std::cin >> this->_select;
		std::cin.get();
		system("cls");
		if (_select > 0 && (size_t)_select < _count + 1) {
			this->_items[_select - 1].Run();
			system("pause");
		}
		else if (_select > 0 && (size_t)_select == _count + 1) {
			this->_running = false;
			this->_select = -1;
		}
		else {
			std::cout << "Entered incorrect number" << std::endl;
			system("pause");
		}
	} while (this->_running);
}
