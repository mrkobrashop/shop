#pragma once
#include "ItemMenu.h"
#include "../String/String.h"
#include <iostream>

class Menu {
public:
	Menu(const char* title, size_t count, ItemMenu* items);
	Menu();
	Menu(const Menu& menu);
	int GetSelect();
	bool GetRunning();
	String GetTitle();
	size_t GetCount();
	ItemMenu* GetItems();
	friend std::ostream& operator << (std::ostream& out, Menu& menu);
	void ProcessMenu();
private:
	int _select;
	bool _running;
	String _title;
	size_t _count;
	ItemMenu* _items;
};
