#include "Error.h"
#include <iostream>
using namespace std;

Error::Error(String errorMsg) : m_errorMsg(errorMsg) {}

void Error::Print()
{
	cout << m_errorMsg << endl;
}