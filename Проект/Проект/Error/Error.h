#pragma once
#include "../String/String.h"

class Error {
public:
	Error(String errorMsg);
	void Print();
private:
	String m_errorMsg;
};