#pragma once
#include "../../Global/Global.h"
#include "../DataBase.h"
#include <iostream>

bool SortUser(const char* fieldName, const char* format, DataBase<User>* head);
bool SortProduct(const char* fieldName, const char* format, DataBase<Product>* head);
bool SortSale(const char* fieldName, const char* format, DataBase<Sale>* head);

template <typename T>
void swap(T* first, T* second);
