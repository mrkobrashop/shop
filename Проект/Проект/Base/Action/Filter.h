#pragma once
#include "../../Global/Global.h"
#include "../DataBase.h"
#include <iostream>

DataBase<User>* FilterUser(DataBase<User>* head, char* firstDate, char* lastDate);
DataBase<Product>* FilterProduct(DataBase<Product>* head, double firstPrice, double lastPrice);
DataBase<Sale>* FilterSale(DataBase<Sale>* head, char* firstDate, char* lastDate);