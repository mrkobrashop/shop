#include "Add.h"
#include "../DataBase.h"
#include "../../Error/Error.h"

template <typename T_head, typename T_item>
T_head* Add(T_item* item, T_head* head)
{
	T_head* newItem = new T_head{};
	newItem->_next = nullptr;
	newItem->_item = item;
	T_head* temp = head;
	if (IsEmpty(head)) {
		temp = newItem;
	}
	else {
		T_head* lastItem = GetLast(head);
		lastItem->_next = newItem;
	}
	return temp;
}
template DataBase<User>* Add(User* item, DataBase<User>* head);
template DataBase<Product>* Add(Product* item, DataBase<Product>* head);
template DataBase<Sale>* Add(Sale* item, DataBase<Sale>* head);

DataBase<User>* AddUser(DataBase<User>* head)
{
	String username{}, password{}, name{}, createdDate{};
	unsigned long int id{};
	User* user = new User{};
	if (clientHead == nullptr) {
		id = 1;
	}
	else {
		id = GetLast(clientHead)->_item->GetId() + 1;
	}
	while (username.GetLength() == 1) {
		std::cout << "Input username: ";
		std::cin >> username;
		try {
			if (GetListItem(clientHead, "Username", username) || GetListItem(employeeHead, "Username", username)) {
				throw Error("User with this login already exists");
			}
		}
		catch (Error& error) {
			error.Print();
			username = String();
		}
	}
	while (password.GetLength() == 1) {
		std::cout << "Input password: ";
		std::cin >> password;
	}
	while (name.GetLength() == 1) {
		std::cout << "Input name: ";
		std::cin >> name;
	}
	while (createdDate.GetLength() == 1) {
		std::cout << "Input create date " << std::endl;
		createdDate = InputDate();
	}
	user->Set(id, username.GetBegin(), password.GetBegin(), name.GetBegin(), createdDate.GetBegin());
	DataBase<User>* temp = Add<DataBase<User>, User>(user, head);
	return temp;
}

DataBase<Product>* AddProduct(DataBase<Product>* head) 
{
	String title{}, desc{}, createDate{};
	double price = 0;
	unsigned long int id{};
	if (productHead == nullptr) {
		id = 1;
	}
	else {
		id = GetLast<Product>(head)->_item->_id + 1;
	}
	while (title.GetLength() == 1) {
		std::cout << "Input title: ";
		std::cin >> title;
	}
	while (desc.GetLength() == 1) {
		std::cout << "Input desc: ";
		std::cin >> desc;
	}
	while (createDate.GetLength() == 1) {
		std::cout << "Input create date" << std::endl;
		createDate = InputDate();
	}
	while (price == 0) {
		std::cout << "Input price: ";
		std::cin >> price;
	}
	Product* product = new Product(id, title.GetBegin(), desc.GetBegin(), createDate.GetBegin(), price);
	DataBase<Product>* temp = Add<DataBase<Product>>(product, productHead);
	return temp;
}

DataBase<Sale>* AddSale(DataBase<Sale>* head)
{
	unsigned long int id = 1, userId = 0;
	int productNumber = 0, count = 0;
	double totalPrice = 0;
	String createDate{};
	unsigned long int product{};
	if (productHead != nullptr) {
		id = GetLast(productHead)->_item->_id + 1;
	}
	while (totalPrice == 0) {
		std::cout << "Input total price: ";
		std::cin >> totalPrice;
	}
	while (count == 0) {
		std::cout << "Input count: ";
		std::cin >> count;
	}
	while (createDate.GetLength() == 1) {
		std::cin.get();
		std::cout << "Input create date" << std::endl;
		createDate = InputDate();
	}
	Print(productHead);
	while (productNumber <= 0 || productNumber > GetListSize(productHead)) {
		std::cout << "Input product number: ";
		std::cin >> productNumber;
	}
	product = GetListItem(productHead, String("Number"), productNumber)->_item->_id;
	while (userId == 0) {
		std::cout << "Input user id: ";
		std::cin >> userId;
	}
	Sale* sale = new Sale(id, totalPrice, product, createDate.GetBegin(), count, userId);
	DataBase<Sale>* temp = Add(sale, head);
	return temp;
}