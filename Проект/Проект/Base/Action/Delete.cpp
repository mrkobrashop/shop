#include "Delete.h"
#include "../DataBase.h"

template <typename T>
T* Delete(int number, T* head)
{
	T* _head = head;
	T* temp = head;
	T* itemDelete = nullptr;
	if (number <= GetListSize(head) && number > 0) {
		if (number == 1) {
			itemDelete = _head;
			_head = head->_next;
		}
		else if (number == GetListSize(head)) {
			int count = 1;
			while (temp)
			{
				if (count == number - 1) {
					itemDelete = temp->_next;
					temp->_next = nullptr;
					break;
				}
				count++;
				temp = temp->_next;
			}
		}
		else {
			int count = 1;
			T* previous = nullptr;
			while (temp) {
				if (count == number) {
					itemDelete = temp;
					previous->_next = temp->_next;
					break;
				}
				previous = temp;
				temp = temp->_next;
				count++;
			}
		}
	}
	if (itemDelete != nullptr) {
		delete itemDelete;
	}
	return _head;
}

template DataBase<User>* Delete(int number, DataBase<User>* head);
template DataBase<Product>* Delete(int number, DataBase<Product>* head);
template DataBase<Sale>* Delete(int number, DataBase<Sale>* head);
