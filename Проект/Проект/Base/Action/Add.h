#pragma once
#include "../../Global/Global.h"
#include <iostream>

template <typename T_head, typename T_item>
T_head* Add(T_item* item, T_head* head);

DataBase<User>* AddUser(DataBase<User>* head);

DataBase<Product>* AddProduct(DataBase<Product>* head);

DataBase<Sale>* AddSale(DataBase<Sale>* head);