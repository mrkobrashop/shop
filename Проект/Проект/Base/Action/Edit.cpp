#include "Edit.h"
#include "../../String/String.h"
using namespace std;
bool EditUser(int number, DataBase<User>* head)
{
	DataBase<User>* EditItem = GetListItem(head, String("Number"),  number);
	int numberField = 0;
	unsigned long int id = 0;
	String meta{};
	bool success = true;
	cout << "1. ID" << endl;
	cout << "2. Username" << endl;
	cout << "3. Password" << endl;
	cout << "4. Name" << endl;
	cout << "5. Create date" << endl;
	cout << "6. Exit" << endl;
	cout << "- > ";
	cin >> numberField;
	if (numberField == 1) {
		while (id <= 0) {
			cout << "Input new value: ";
			cin >> id;
		}
	}
	else if(numberField < 6) {
		cin.get();
		while (meta.GetLength() == 1) {
			cout << "Input new value: ";
			cin >> meta;
		}
	}
	switch (numberField)
	{
	case 1:
		EditItem->_item->SetId(id);
		break;
	case 2:
		EditItem->_item->SetMeta(meta.GetBegin(), "Username");
		break;
	case 3:
		EditItem->_item->SetMeta(meta.GetBegin(), "Password");
		break;
	case 4:
		EditItem->_item->SetMeta(meta.GetBegin(), "Name");
		break;
	case 5:
		EditItem->_item->SetMeta(meta.GetBegin(), "Create date");
		break;
	case 6:
		break;
	default:
		cout << "You entered incorrect number" << endl;
		success = false;
		break;
	}
	return success;
}

bool EditProduct(int number, DataBase<Product>* head)
{
	DataBase<Product>* productEdit = GetListItem(head, String("Number"), number);
	char numberField{};
	bool success = true;
	cout << "1. ID" << endl;
	cout << "2. Title" << endl;
	cout << "3. Description" << endl;
	cout << "4. Price" << endl;
	cout << "5. Create date" << endl;
	cout << "6. Exit" << endl;
	cout << "- > ";
	cin >> numberField;
	if (numberField == '1') {
		unsigned long int id{};
		cout << "Input new value: ";
		cin >> id;
		productEdit->_item->_id = id;
	}
	else if (numberField == '2') {
		String meta{};
		cout << "Input new value: ";
		cin.get();
		cin >> meta;
		productEdit->_item->_title = meta;
	}
	else if (numberField == '3') {
		String meta{};
		cout << "Input new value: ";
		cin.get();
		cin >> meta;
		productEdit->_item->_desc = meta;
	}
	else if (numberField == '4') {
		double price{};
		cout << "Input new value: ";
		cin >> price;
		productEdit->_item->_price = price;
	}
	else if (numberField == '5') {
		String meta{};
		cout << "Input new value" << endl;
		cin.get();
		cin >> meta;
		productEdit->_item->_createDate = meta;
	}
	else if (numberField > '6') {
		cout << "Entered incorrect number" << endl;
		success = false;
	}
	return success;
}

bool EditSale(int number, DataBase<Sale>* head)
{
	DataBase<Sale>* saleEdit = GetListItem(head, String("Number"), number);
	char numberField{};
	bool success = true;
	cout << "1. ID" << endl;
	cout << "2. Total price" << endl;
	cout << "3. Create date" << endl;
	cout << "4. Product" << endl;
	cout << "5. Count" << endl;
	cout << "6. User ID" << endl;
	cout << "7. Exit" << endl;
	cout << "- > ";
	cin >> numberField;
	if (numberField == '1') {
		unsigned long int id{};
		cout << "Input new value: ";
		cin >> id;
		saleEdit->_item->_id = id;
	}
	else if (numberField == '2') {
		double totalPrice{};
		cout << "Input new value: ";
		cin >> totalPrice;
		saleEdit->_item->_totalPrice = totalPrice;
	}
	else if (numberField == '3') {
		String meta;
		cout << "Input new value: ";
		cin.get();
		cin >> meta;
		saleEdit->_item->_createDate = meta;
	}
	else if (numberField == '4') {
		Print(productHead);
		int productNumber{};
		do {
			cin >> productNumber;
			if (productNumber < 0 || productNumber > GetListSize(productHead)) {
				cout << "Entered incorrect number" << endl;
				cout << "Try again: ";
			}
		} while (productNumber < 0 || productNumber > GetListSize(productHead));
		saleEdit->_item->_productId = GetListItem(productHead, String("Number"), productNumber)->_item->_id;
	}
	else if (numberField == '5') {
		int count = 0;
		while (count == 0) {
			cout << "Input product count: ";
			cin >> count;
		}
		saleEdit->_item->_count = count;
	}
	else if (numberField == '6') {
		unsigned long int userId = 0;
		while (userId <= 0) {
			std::cout << "Input user id: ";
			std::cin >> userId;
		}
		saleEdit->_item->_userId = userId;
	}
	else if(numberField >= '7'){
		cout << "Entered incorrect number" << endl;
		success = false;
	}
	return success;
}