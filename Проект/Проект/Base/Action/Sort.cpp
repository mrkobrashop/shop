#include "Sort.h"
using namespace std;

bool SortUser(const char* fieldName, const char* format, DataBase<User>* head)
{
	String type(format);
	String field(fieldName);
	DataBase<User>* current = head;
	bool success = true;
	if (field == "ID") {
		while (current) {
			DataBase<User>* temp = head;
			while (temp) {
				if (type == "ASC") {
					if (current->_item->GetId() > temp->_item->GetId()) {
						swap <DataBase<User>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (current->_item->GetId() < temp->_item->GetId()) {
						swap <DataBase<User>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else if (field == "Name" || field == "Create date") {
		while (current) {
			DataBase<User>* temp = head;
			while (temp) {
				String first = current->_item->GetHumanMeta(fieldName);
				String second = temp->_item->GetHumanMeta(fieldName);
				bool flag{};
				if (type == "ASC") {
					if (first > second) {
						swap <DataBase<User>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (first < second) {
						swap <DataBase<User>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else {
		cout << "Entered incorrect field name" << endl;
		cout << "Try again" << endl;
		success = false;
	}
	return success;
}
bool SortProduct(const char* fieldName, const char* format, DataBase<Product>* head)
{
	String type = format;
	String field = fieldName;
	DataBase<Product>* current = head;
	bool success = true;
	if (field == "ID") {
		while (current) {
			DataBase<Product>* temp = head;
			while (temp) {
				if (type == "ASC") {
					if (current->_item->_id > temp->_item->_id) {
						swap <DataBase<Product>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (current->_item->_id < temp->_item->_id) {
						swap <DataBase<Product>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else if (field == "Title" || field == "Create date") {
		while (current) {
			DataBase<Product>* temp = head;
			while (temp) {
				String first{}, second{};
				if (field == "Title") {
					first = current->_item->_title;
					second = temp->_item->_title;
				}
				else if (field == "Create date") {
					first = current->_item->_createDate;
					second = temp->_item->_createDate;
				}
				if (type == "ASC") {
					if (first > second) {
						swap <DataBase<Product>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (first < second) {
						swap <DataBase<Product>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else if (field == "Price") {
		while (current) {
			DataBase<Product>* temp = head;
			while (temp) {
				double first = current->_item->_price;
				double second = temp->_item->_price;
				if (type == "ASC") {
					if (first > second) {
						swap <DataBase<Product>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (first < second) {
						swap <DataBase<Product>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else {
		cout << "Entered incorrect field name" << endl;
		cout << "Try again" << endl;
		success = false;
	}
	return success;
}
bool SortSale(const char* fieldName, const char* format, DataBase<Sale>* head)
{
	String type = format;
	String field = fieldName;
	DataBase<Sale>* current = head;
	bool success = true;
	if (field == "ID") {
		while (current) {
			DataBase<Sale>* temp = head;
			while (temp) {
				if (type == "ASC") {
					if (current->_item->_id > temp->_item->_id) {
						swap <DataBase<Sale>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (current->_item->_id < temp->_item->_id) {
						swap <DataBase<Sale>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else if (field == "Create date") {
		while (current) {
			DataBase<Sale>* temp = head;
			while (temp) {
				String first = current->_item->_createDate;
				String second = temp->_item->_createDate;
				if (type == "ASC") {
					if (first > second) {
						swap <DataBase<Sale>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (first < second) {
						swap <DataBase<Sale>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else if (field == "Total price") {
		while (current) {
			DataBase<Sale>* temp = head;
			while (temp) {
				double first = current->_item->_totalPrice;
				double second = temp->_item->_totalPrice;
				if (type == "ASC") {
					if (first > second) {
						swap <DataBase<Sale>>(current, temp);
					}
				}
				else if (type == "DESC") {
					if (first < second) {
						swap <DataBase<Sale>>(current, temp);
					}
				}
				temp = temp->_next;
			}
			current = current->_next;
		}
	}
	else {
		cout << "Entered incorrect field name" << endl;
		cout << "Try again" << endl;
		success = false;
	}
	return success;
}

template <typename T>
void swap(T* first, T* second)
{
	T* temp = new T{};
	temp->_item = first->_item;
	first->_item = second->_item;
	second->_item = temp->_item;
}

template void swap(DataBase<User>* first, DataBase<User>* second);
template void swap(DataBase<Product>* first, DataBase<Product>* second);
template void swap(DataBase<Sale>* first, DataBase<Sale>* second);
