#pragma once
#include "../../Global/Global.h"
#include "../DataBase.h"
#include <iostream>

bool EditUser(int number, DataBase<User>* head);
bool EditProduct(int number, DataBase<Product>* head);
bool EditSale(int number, DataBase<Sale>* head);