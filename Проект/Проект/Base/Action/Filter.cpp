#include "Filter.h"
#include "Add.h"

DataBase<User>* FilterUser(DataBase<User>* head, char* firstDate, char* lastDate)
{
	DataBase<User>* tempHead = nullptr;
	String first = firstDate;
	String last = lastDate;
	DataBase<User>* _head = head;
	while (_head)
	{
		String itemDate = _head->_item->GetHumanMeta("Create date");
		if (itemDate >= first && itemDate <= last) {
			tempHead = Add <DataBase<User>, User>(_head->_item, tempHead);
		}
		_head = _head->_next;
	}
	return tempHead;
}

DataBase<Product>* FilterProduct(DataBase<Product>* head, double firstPrice, double lastPrice)
{
	DataBase<Product>* tempHead = nullptr;
	DataBase<Product>* _head = head;
	while (_head)
	{
		double itemPrice = _head->_item->_price;
		if (itemPrice >= firstPrice && itemPrice <= lastPrice) {
			tempHead = Add <DataBase<Product>, Product>(_head->_item, tempHead);
		}
		_head = _head->_next;
	}
	return tempHead;
}
DataBase<Sale>* FilterSale(DataBase<Sale>* head, char* firstDate, char* lastDate)
{
	DataBase<Sale>* tempHead = nullptr;
	String first = firstDate;
	String last = lastDate;
	DataBase<Sale>* _head = head;
	while (_head)
	{
		String itemDate = _head->_item->_createDate;
		if (itemDate >= first && itemDate <= last) {
			tempHead = Add <DataBase<Sale>, Sale>(_head->_item, tempHead);
		}
		_head = _head->_next;
	}
	return tempHead;
} 