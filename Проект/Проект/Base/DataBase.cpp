#include "DataBase.h"
#include "../User/User.h"
#include "../Product/Product.h"
#include "../Product/Sale.h"
#include <type_traits>

template <typename T>
bool IsEmpty(DataBase<T>* head) {
	return (head == nullptr);
}
template bool IsEmpty(DataBase<User>* head);
template bool IsEmpty(DataBase<Product>* head);
template bool IsEmpty(DataBase<Sale>* head);

template <typename T>
DataBase<T>* GetLast(DataBase<T>* head)
{
	DataBase<T>* temp = head;
	DataBase<T>* last = nullptr;
	if (!IsEmpty<T>(head)) {
		while (temp)
		{
			if (temp->_next == nullptr) {
				last = temp;
			}
			temp = temp->_next;
		}
	}
	delete temp;
	return last;
}
template DataBase<User>* GetLast(DataBase<User>* head);
template DataBase<Product>* GetLast(DataBase<Product>* head);
template DataBase<Sale>* GetLast(DataBase<Sale>* head);

template <typename T>
int GetListSize(DataBase<T>* head)
{
	DataBase<T>* temp = head;
	int count{};
	while (temp)
	{
		count++;
		temp = temp->_next;
	}
	return count;
}
template int GetListSize(DataBase<User>* head);
template int GetListSize(DataBase<Product>* head);
template int GetListSize(DataBase<Sale>* head);

template <typename T>
void Print(DataBase<T>* head)
{
	DataBase<T>* temp = head;
	for (int i = 1; temp; i++)
	{
		std::cout << i << ". " << *temp->_item << std::endl;
		temp = temp->_next;
	}
}
template void Print(DataBase<User>* head);
template void Print(DataBase<Product>* head);
template void Print(DataBase<Sale>* head);

template <typename T>
DataBase<T>* GetListItem(DataBase<T>* head, String field, int item)
{
	DataBase<T>* temp = head;
	DataBase<T>* result = nullptr;
	int count = 1;
	while (temp) {
		if (field == "Number") {
			if (count == item) {
				result = temp;
				break;
			}
			count++;
		}
		temp = temp->_next;
	}
	return result;
}
template DataBase<User>* GetListItem(DataBase<User>* head, String field, int item);
template DataBase<Product>* GetListItem(DataBase<Product>* head, String field, int item);
template DataBase<Sale>* GetListItem(DataBase<Sale>* head, String field, int item);

template <>
DataBase<Product>* GetListItem(DataBase<Product>* head, String field, String value)
{
	DataBase<Product>* temp = head;
	DataBase<Product>* result = nullptr;
	while (temp) {
		if (field == "Title") {
			if (value == temp->_item->_title) {
				result = temp;
			}
		} 
		temp = temp->_next;
	}
	delete temp;
	return result;
}

template <>
DataBase<User>* GetListItem(DataBase<User>* head, String field, String value)
{
	DataBase<User>* temp = head;
	DataBase<User>* result = nullptr;
	while (temp) {
		if (field == "Username") {
			if (value == temp->_item->GetHumanMeta("Username")) {
				result = temp;
			}
		}
		temp = temp->_next;
	}
	delete temp;
	return result;
}

template <>
DataBase<Product>* GetListItem(DataBase<Product>* head, String field, unsigned long int value)
{
	DataBase<Product>* temp = head;
	DataBase<Product>* result = nullptr;
	while (temp) {
		if (field == "ID") {
			if (value == temp->_item->_id) {
				result = temp;
			}
		}
		temp = temp->_next;
	}
	delete temp;
	return result;
}