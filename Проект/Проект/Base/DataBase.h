#pragma once
#include "../String/String.h"


template <class T>
struct DataBase {
	DataBase<T>* _next;
	T* _item;
};

template <typename T>
bool IsEmpty(DataBase<T>* head);

template <typename T>
DataBase<T>* GetLast(DataBase<T>* head);

template <typename T>
int GetListSize(DataBase<T>* head);

template <typename T>
DataBase<T>* GetListItem(DataBase<T>* head, String field, int item);

template <typename T>
void Print(DataBase<T>* head); 

template <typename T>
DataBase<T>* GetListItem(DataBase<T>* head, String field, String value);

template <typename T>
DataBase<T>* GetListItem(DataBase<T>* head, String field, unsigned long int id);
