#pragma once
#include "../DataBase.h"
#include "../../Global/Global.h"

void SaveString(const char* filePath, const char* str, int position = 0);

void SaveReport(DataBase<User>* head, const char* filePath);

void SaveReport(DataBase<Product>* head, const char* filePath);

void SaveReport(DataBase<Sale>* head, const char* filePath);

