#include "BaseReport.h"
#include "../DataBase.h"
#include <fstream>

using namespace std;

void SaveString(const char* filePath, const char* str, int position)
{
	ofstream out;
	if (position) {
		out.open(filePath);
	}
	else {
		out.open(filePath, ios::app);
	}
	if (out.is_open()) {
		out << str << endl;
	}
	out.close();
}

void SaveReport(DataBase<User>* head, const char* filePath)
{
	DataBase<User>* temp = head;
	int count = 0;
	while (temp) {
		String str{};
		char* id = new char[256]{};
		sprintf_s(id, 256, "%d", temp->_item->GetId());
		str = str + id + ";";
		str = str + temp->_item->GetHumanMeta("Username").GetBegin() + ";";
		str = str + temp->_item->GetHumanMeta("Password").GetBegin() + ";";
		str = str + temp->_item->GetHumanMeta("Name").GetBegin() + ";";
		str = str + temp->_item->GetHumanMeta("Create date").GetBegin();
		if (count == 0) {
			SaveString(filePath, str.GetBegin(), 1);
		}
		else {
			SaveString(filePath, str.GetBegin());
		}
		delete[] id;
		temp = temp->_next;
		count++;
	}
}

void SaveReport(DataBase<Product>* head, const char* filePath)
{
	DataBase<Product>* temp = head;
	int count = 0;
	while (temp) {
		String str{};
		char* id = new char[256]{}; 
		char* price = new char[256]{};
		sprintf_s(id, 256, "%d", temp->_item->_id);
		sprintf_s(price, 256, "%g", temp->_item->_price);
		str = str + id + ";";
		str = str + temp->_item->_title.GetBegin() + ";";
		str = str + temp->_item->_desc.GetBegin() + ";";
		str = str + temp->_item->_createDate.GetBegin() + ";";
		str = str + price;
		if (count == 0) {
			SaveString(filePath, str.GetBegin(), 1);
		}
		else {
			SaveString(filePath, str.GetBegin());
		}
		delete[] id;
		delete[] price;
		temp = temp->_next;
		count++;
	}
}

void SaveReport(DataBase<Sale>* head, const char* filePath)
{
	DataBase<Sale>* temp = head;
	int count = 0;
	while (temp) {
		String str{};
		char* id = new char[256]{}, * price = new char[256]{}, * productId = new char[256]{}, 
			* productCount = new char[256]{}, * userId = new char[256]{};
		sprintf_s(id, 256, "%d", temp->_item->_id);
		sprintf_s(price, 256, "%g", temp->_item->_totalPrice);
		sprintf_s(productId, 256, "%d", temp->_item->_productId);
		sprintf_s(productCount, 256, "%d", temp->_item->_count);
		sprintf_s(userId, 256, "%d", temp->_item->_userId);
		str = str + id + ";";
		str = str + price + ";";
		str = str + productId + ";";
		str = str + temp->_item->_createDate.GetBegin() + ";";
		str = str + productCount + ";";
		str = str + userId;
		if (count == 0) {
			SaveString(filePath, str.GetBegin(), 1);
		}
		else {
			SaveString(filePath, str.GetBegin());
		}
		delete[] id, delete[] price, delete[] productId, delete[] productCount, delete[] userId;
		count++;
		temp = temp->_next;
	}
}

