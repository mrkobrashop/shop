#include "Global.h"
#include <iostream>
#include "../Error/Error.h"
using namespace std; 

DataBase<User>* clientHead = nullptr;
DataBase<User>* employeeHead = nullptr;
DataBase<Product>* productHead = nullptr;
DataBase<Sale>* saleHead = nullptr;
Auth* login = new Auth{};

String InputDate() {
	String year{}, month{}, day{}, result{};
	while (year.GetLength() == 1) {
		cout << "Input the year in the format XXXX: ";
		cin >> year;
		try {
			if (year.GetLength() != 5) {
				throw Error("The year must contain 4 characters");
			}
			else {
				for (size_t i = 0; i < year.GetLength() - 1; i++)
				{
					if ((int)year[i] < 48 || (int)year[i] > 57) {
						throw Error("Year can only contain numbers");
						break;
					}
				}
			}
		}
		catch (Error& error) {
			error.Print();
			year = String();
		}
	}
	while (month.GetLength() == 1) {
		cout << "Input the month in the format XX: ";
		cin >> month;
		try {
			if (month.GetLength() != 3) {
				throw Error("The month must contain 2 characters");
			}
			else {
				for (size_t i = 0; i < month.GetLength() - 1; i++)
				{
					if ((int)month[i] < 48 || (int)month[i] > 57) {
						throw Error("Year can only contain numbers");
						break;
					}
				}
			}
		}
		catch (Error& error) {
			error.Print();
			month = String();
		}
	}
	while (day.GetLength() == 1) {
		cout << "Input the day in the format XX: ";
		cin >> day;
		try {
			if (day.GetLength() != 3) {
				throw Error("The day must contain 2 characters");
			}
			else {
				for (size_t i = 0; i < day.GetLength() - 1; i++)
				{
					if ((int)day[i] < 48 || (int)day[i] > 57) {
						throw Error("Year can only contain numbers");
						break;
					}
				}
			}
		}
		catch (Error& error) {
			error.Print();
			day = String();
		}
	}
	result = year + "-" + month.GetBegin() + "-" + day.GetBegin();
	return result;
}
