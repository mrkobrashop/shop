#pragma once
#include "../Base/DataBase.h"
#include "../Product/Product.h"
#include "../Product/Sale.h"
#include "../Screens/Auth/Auth.h"
#include "../Screens/Edit/Editor.h"
#include "../Screens/Shop/Shop.h"
#include "../Screens/Report/Report.h"
#include "../Screens/State/State.h"

extern DataBase<User>* clientHead;
extern DataBase<User>* employeeHead;
extern DataBase<Product>* productHead;
extern DataBase<Sale>* saleHead;
extern Auth* login;

String InputDate();