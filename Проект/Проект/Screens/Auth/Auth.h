#pragma once
#include "../InterfaceScreen.h"
#include "../../User/User.h"
#include "../../String/String.h"

class Auth : public InterfaceScreen {
public:
	Auth();
	Auth(const Auth& auth);
	Auth(bool auth, User* user);
	User* CheckUser(String username, String password);
	Auth operator= (User* user);
	bool InputUser();
	bool GetAuth();
	User* GetUser();
	int Start(int screen);
	void RenderMain() const;
private: 
	bool _auth;
	User* _user;
};

int Authorization();