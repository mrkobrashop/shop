#include "Auth.h"
#include "../../Global/Global.h"
#include "../../Base/DataBase.h"
#include "../../Error/Error.h"

Auth::Auth() : _auth(false), _user(nullptr) {}

Auth::Auth(bool auth, User* user) : _auth(auth), _user(user) {}

Auth::Auth(const Auth& auth) : _auth(auth._auth), _user(auth._user) {}

Auth Auth::operator= (User* user)
{
	_user = user;
	_auth = true;
	return *this;
}

int Auth::Start(int screen)
{
	if (screen == 0) {
		this->RenderMain();
		if (InputUser()) {
			std::cout << "You have successfully logged in" << std::endl;
		}
		else {
			std::cout << "Auth has failed" << std::endl;
		}
	}
	else {
		std::cout << "You have already authored" << std::endl;
	}
	return 0;
}

void Auth::RenderMain() const
{
	std::cout << "Authorization" << std::endl;
}

bool Auth::InputUser()
{
	String username{}, password{}, choose{}, msg;
	User* user{};
	bool run = true;
	do {
		try {
			std::cout << "Input username: ";
			std::cin >> username;
			if (username.GetLength() == 1) {
				throw Error("Input invalid username");
			}
			std::cout << "Input password: ";
			std::cin >> password;
			if (password.GetLength() == 1) {
				throw Error("Input invalid password");
			}
			user = CheckUser(username, password);
			_auth = true;
			_user = user;
			run = false;
		}
		catch (Error& error) {
			error.Print();
			std::cout << "Try again?" << std::endl;
			std::cout << "1. Yes\n2. No" << std::endl;
			std::cin >> choose;
			if (choose == "2") {
				run = false;
			}
		}
 	} while (run);
	return _auth;
}

User* Auth::CheckUser(String username, String password)
{
	User* user = nullptr;
	if (GetListItem(clientHead, String("Username"), username)) {
		user = GetListItem(clientHead, String("Username"), username)->_item;
	}
	else if (GetListItem(employeeHead, String("Username"), username)) {
		user = GetListItem(employeeHead, String("Username"), username)->_item;
	}
	if (user) {
		if (password != user->GetHumanMeta("Password")) {
			user = nullptr;
			throw Error("Invalid password");
		}
	}
	else {
		throw Error("There is no user with this login");
	}
	return user;
}

bool Auth::GetAuth()
{
	return _auth;
}

int Authorization()
{
	int screen = 0;
	if (login->GetAuth()) {
		screen = 1;
	}
	login->Start(screen);
	return 0;
}

User* Auth::GetUser()
{
	return _user;
}