#include "Report.h"
#include "../../String/String.h"
#include "../../Base/Report/BaseReport.h"
#include <iostream>

using namespace std;
int Report::Start(int screen) 
{
	String item{};
	String message{};
	this->RenderMain();
	cout << "- > ";
	cin >> item;
	if (item == "1") {
		SaveReport(clientHead, "Files/Client.txt");
		message = "File saved successfully";
	}
	else if (item == "2") {
		SaveReport(clientHead, "Files/Employee.txt");
		message = "File saved successfully";
	}
	else if (item == "3") {
		SaveReport(productHead, "Files/Product.txt");
		message = "File saved successfully";
	}
	else if (item == "4") {
		SaveReport(saleHead, "Files/Sale.Txt");
		message = "File saved successfully";
	}
	else if (item == "5") {
		message = "Exit";
	}
	else {
		message = "Input incorrect number";
	}
	cout << message << endl;
	return 0;
}

void Report::RenderMain() const
{
	cout << "Report list" << endl;
	cout << "1. Client" << endl;
	cout << "2. Employee" << endl;
	cout << "3. Product" << endl;
	cout << "4. Sale" << endl;
	cout << "5. Exit" << endl;
}

int ReportNav()
{
	Report report{};
	report.Start(0);
	return 0;
}