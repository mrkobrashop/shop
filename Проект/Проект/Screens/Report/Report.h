#include "../InterfaceScreen.h"
#pragma once

class Report : public InterfaceScreen {
public:
	int Start(int screen);
	void RenderMain() const;
};

int ReportNav();