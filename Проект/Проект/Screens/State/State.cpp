#include "State.h"
#include "../../Base/Action/Add.h"
#include <iostream>
#include <fstream>

int StateNav()
{
	State state{};
	state.Start(0); 
	return 0;
}

int	State::Start(int screen)
{
	this->RenderMain();
	this->Save();
	return 0;
}

void State::RenderMain() const
{
	std::cout << "State preservation..." << std::endl;
}

void State::Save()
{
	SaveReport(clientHead, "Base/Files/Client.txt");
	SaveReport(employeeHead, "Base/Files/Employee.txt");
	SaveReport(productHead, "Base/Files/Product.txt");
	SaveReport(saleHead, "Base/Files/Sale.txt");
}

void State::Upload()
{
	clientHead = this->UploadUser("Base/Files/Client.txt");
	employeeHead = this->UploadUser("Base/Files/Employee.txt");
	productHead = this->UploadProduct("Base/Files/Product.txt");
	saleHead = this->UploadSale("Base/Files/Sale.txt");
}

DataBase<User>* State::UploadUser(const char* filePath)
{
	DataBase<User>* temp = nullptr;
	std::ifstream in;
	in.open(filePath);
	if (in.is_open()) {
		while (!in.eof()) {
			String str{};
			in >> str;
			String* data = str.Splite(';');
			if (data) {
				User* user = new User(atoi(data[0].GetBegin()), data[1].GetBegin(), data[2].GetBegin(), data[3].GetBegin(), data[4].GetBegin());
				temp = Add(user, temp);
			}
		}
	}
	return temp;
}

DataBase<Product>* State::UploadProduct(const char* filePath)
{
	DataBase<Product>* temp = nullptr;
	std::ifstream in;
	in.open(filePath);
	if (in.is_open()) {
		while (!in.eof()) {
			String str{};
			in >> str;
			String* data = str.Splite(';');
			if (data) {
				Product* product = new Product(atoi(data[0].GetBegin()), data[1].GetBegin(), data[2].GetBegin(), data[3].GetBegin(), atof(data[4].GetBegin()));
				temp = Add(product, temp);
			}
		}
	}
	return temp;
}

DataBase<Sale>* State::UploadSale(const char* filePath)
{
	DataBase<Sale>* temp = nullptr;
	std::ifstream in;
	in.open(filePath);
	if (in.is_open()) {
		while (!in.eof()) {
			String str{};
			in >> str;
			std::cout << str << std::endl;
			String* data = str.Splite(';');
			if (data) {
				Sale* sale = new Sale(atoi(data[0].GetBegin()),
					atof(data[1].GetBegin()), atoi(data[2].GetBegin()), data[3].GetBegin(),
					atoi(data[4].GetBegin()), atoi(data[5].GetBegin()));
				temp = Add(sale, temp);
			}
		}
	}
	return temp;
}