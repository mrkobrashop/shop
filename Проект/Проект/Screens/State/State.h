#pragma once 
#include "../InterfaceScreen.h"
#include "../../Base/Report/BaseReport.h"

class State : public InterfaceScreen {
public:
	int Start(int screen);
	void RenderMain() const;
	void Save();
	void Upload();
	DataBase<User>* UploadUser(const char* filePath);
	DataBase<Product>* UploadProduct(const char* filePath);
	DataBase<Sale>* UploadSale(const char* filePath);
};

int StateNav();