#pragma once
class InterfaceScreen {
public:
	virtual int Start(int screen) = 0;
	virtual void RenderMain() const = 0;
};