#pragma once
#include "../InterfaceScreen.h"
#include "../../Product/Sale.h"

class Shop : public InterfaceScreen {
public:
	int Start(int screen);
	void RenderMain() const;
	int BuyProduct(int product);
	void PrintOrder(const Sale& sale);
};

int ShopNav();