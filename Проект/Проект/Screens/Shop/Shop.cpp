#include "Shop.h"
#include "../../Global/Global.h"
#include "../../Product/Product.h"
#include "../../Product/Sale.h"
#include "../../Base/Action/Add.h"
#include "../../Error/Error.h"
#include <iostream>
#include "ctime"

int Shop::Start(int screen)
{
	bool run = true;
	while (run) {
		int productNumber = 0;
		system("cls");
		this->RenderMain();
		if (screen == 0) {
			while (productNumber == 0 || productNumber > GetListSize(productHead) + 1)
			{
				std::cout << "Input the product number which you want to buy: ";
				std::cin >> productNumber;
				std::cin.get();
			}
			if (productNumber == GetListSize(productHead) + 1) {
				run = false;
			}
			else {
				this->BuyProduct(productNumber);
				system("pause");
			}
		}
		else {
			std::cout << "Autorization to make a purchase" << std::endl;
			run = false;
		}
	}
	return 0;
}

void Shop::RenderMain() const
{
	std::cout << "Produt list" << std::endl;
	Print(productHead);
	std::cout << GetListSize(productHead) + 1 << ". Exit" << std::endl;
}

int Shop::BuyProduct(int product)
{
	unsigned long int id = 1, userId = 0;
	char* createDate = new char[20];
	String countInput{};
	int count = 0;
	double totalPrice = 0;
	Product* item = GetListItem(productHead, String("Number"), product)->_item;
	if (saleHead != nullptr) {
		id = GetLast(saleHead)->_item->_id + 1;
	}
	while (countInput.GetLength() == 1) {
		try {
			std::cout << "Input product count: ";
			std::cin >> countInput;
			count = atoi(countInput.GetBegin());
			if (count <= 0) {
				throw Error("Input a number greater than zero");
			} 
		}
		catch (Error& error) {
			error.Print();
			countInput = String();
			count = 0;
		}
	}
	totalPrice = count * item->_price;
	userId = login->GetUser()->GetId();
	time_t now = time(0);
	struct tm date;
	localtime_s(&date, &now);
	const char* format = "%Y-%m-%d";
	strftime(createDate, 20, format, &date);
	Sale* sale = new Sale(id, totalPrice, item->_id, createDate, count, userId);
	saleHead = Add(sale, saleHead);
	this->PrintOrder(*sale);
	return 0;
}

void Shop::PrintOrder(const Sale& sale)
{
	Product* product = GetListItem(productHead, String("ID"), sale._productId)->_item;
	std::cout << "Product title: " << *product << std::endl;
	std::cout << "Count: " << sale._count << std::endl;
	std::cout << "Total price: " << sale._totalPrice << std::endl;
	std::cout << "Order date: " << sale._createDate << std::endl;
}

int ShopNav()
{
	Shop shop{};
	int screen = 0;
	if (!(login->GetAuth())) {
		screen = 1;
	}
	shop.Start(screen);
	return 0;
}
