#include "Editor.h"
#include "../../String/String.h"
#include "../../User/User.h"
#include "../../Global/Global.h"
#include "../../Base/Action/Add.h"
#include "../../Base/Action/Delete.h"
#include "../../Base/Action/Edit.h"
#include "../../Base/Action/Filter.h"
#include "../../Base/Action/Sort.h"
#include "../../Error/Error.h"
#include <iostream>

int Editor::Start(int screen)
{
	String action{}, object{};
	this->PrintItemName();
	std::cin >> object;
	system("cls");
	if (object == "1") {
		this->RenderMain();
		std::cin >> action;
		system("cls");
		if (action == "1") {
			clientHead = AddUser(clientHead);
			std::cout << "User successfully added" << std::endl;
		}
		else if (action == "2") {
			Print(clientHead);
			int itemDelete = 0;
			try {
				std::cout << "Input delete number: ";
				std::cin >> itemDelete;
				if (itemDelete <= 0 || itemDelete > GetListSize(clientHead)) {
					throw Error("User with this number does not exist");
				}
				clientHead = Delete(itemDelete, clientHead);
				std::cout << "User successfully delete" << std::endl;
			}
			catch (Error& error) {
				error.Print();
			}
		}
		else if (action == "3") {
			Print(clientHead);
			int itemEdit = 0;
			while (itemEdit <= 0 || itemEdit > GetListSize(clientHead)) {
				std::cout << "Input edit number: ";
				std::cin >> itemEdit;
			}
			if (EditUser(itemEdit, clientHead)) {
				std::cout << "User successfully edited" << std::endl;
			}
		}
		else if (action == "4") {
			String firstDate{}, lastDate{};
			while (firstDate.GetLength() == 1) {
				std::cout << "Input first date: ";
				std::cin >> firstDate;
			}
			while (lastDate.GetLength() == 1) {
				std::cout << "Input last date: ";
				std::cin >> lastDate;
			}
			clientHead = FilterUser(clientHead, firstDate.GetBegin(), lastDate.GetBegin());
			if (clientHead) {
				std::cout << "User successfully filtered" << std::endl;
				Print(clientHead);
			}
			else {
				std::cout << "No one item was not selected" << std::endl;
			}
		}
		else if (action == "5") {
			String field{}, format{};
			std::cout << "ID\nName\nCreate date\nInput field name sort: ";
			std::cin >> field;
			std::cout << "ASC\nDESC\nInput sort order: ";
			std::cin >> format;
			if (SortUser(field.GetBegin(), format.GetBegin(), clientHead)) {
				std::cout << "User successfuly sort" << std::endl;
			}
		}
		else if (action == "6") {
			Print(clientHead);
		}
	}
	else if (object == "2") {
		this->RenderMain();
		std::cin >> action;
		system("cls");
		if (action == "1") {
			employeeHead = AddUser(employeeHead);
			std::cout << "User successfully added" << std::endl;
		}
		else if (action == "2") {
			Print(employeeHead);
			int itemDelete = 0;
			try {
				std::cout << "Input delete number: ";
				std::cin >> itemDelete;
				if (itemDelete <= 0 || itemDelete > GetListSize(employeeHead)) {
					throw Error("User with this number does not exist");
				}
				employeeHead = Delete(itemDelete, employeeHead);
				std::cout << "User successfully delete" << std::endl;
			}
			catch (Error& error) {
				error.Print();
			}
		}
		else if (action == "3") {
			Print(employeeHead);
			int itemEdit = 0;
			while (itemEdit <= 0 || itemEdit > GetListSize(employeeHead)) {
				std::cout << "Input edit number: ";
				std::cin >> itemEdit;
			}
			if (EditUser(itemEdit, employeeHead)) {
				std::cout << "User successfully edited" << std::endl;
			}
		}
		else if (action == "4") {
			String firstDate{}, lastDate{};
			while (firstDate.GetLength() == 1) {
				std::cout << "Input first date: ";
				std::cin >> firstDate;
			}
			while (lastDate.GetLength() == 1) {
				std::cout << "Input last date: ";
				std::cin >> lastDate;
			}
			employeeHead = FilterUser(employeeHead, firstDate.GetBegin(), lastDate.GetBegin());
			if (employeeHead) {
				std::cout << "User successfully filtered" << std::endl;
				Print(employeeHead);
			}
			else {
				std::cout << "No one item was not selected" << std::endl;
			}
		}
		else if (action == "5") {
			String field{}, format{};
			std::cout << "ID\nName\nCreate date\nInput field name sort: ";
			std::cin >> field;
			std::cout << "ASC\nDESC\nInput sort order: ";
			std::cin >> format;
			if (SortUser(field.GetBegin(), format.GetBegin(), employeeHead)) {
				std::cout << "User successfuly sort" << std::endl;
			}
		}
		else if (action == "6") {
			Print(employeeHead);
		}
	}
	else if (object == "3") {
		this->RenderMain();
		std::cin >> action;
		system("cls");
		if (action == "1") {
			productHead = AddProduct(productHead);
			Print(productHead);
			std::cout << "Product successfully added" << std::endl;
		}
		else if (action == "2") {
			Print(productHead);
			int itemDelete = 0;
			try {
				std::cout << "Input delete number: ";
				std::cin >> itemDelete;
				if (itemDelete <= 0 || itemDelete > GetListSize(productHead)) {
					throw Error("Product with this number does not exist");
				}
				productHead = Delete(itemDelete, productHead);
				std::cout << "Product successfully delete" << std::endl;
			}
			catch (Error& error) {
				error.Print();
			}
		}
		else if (action == "3") {
			Print(productHead);
			int itemEdit = 0;
			while (itemEdit <= 0 || itemEdit > GetListSize(productHead)) {
				std::cout << "Input edit number: ";
				std::cin >> itemEdit;
			}
			if (EditProduct(itemEdit, productHead)) {
				std::cout << "Product successfully edited" << std::endl;
			}
		}
		else if (action == "4") {
			double firstPrice = 0, lastPrice = 0;
			while (firstPrice == 0) {
				std::cout << "Input first price: ";
				std::cin >> firstPrice;
			}
			while (lastPrice == 0) {
				std::cout << "Input last price: ";
				std::cin >> lastPrice;
			}
			productHead = FilterProduct(productHead, firstPrice, lastPrice);
			if (productHead) {
				std::cout << "Product successfully filtered" << std::endl;
				Print(productHead);
			}
			else {
				std::cout << "No one item was not selected" << std::endl;
			}
		}
		else if (action == "5") {
			String field{}, format{};
			std::cout << "ID\nTitle\nCreate date\nPrice\nInput field name sort: ";
			std::cin >> field;
			std::cout << "ASC\nDESC\nInput sort order: ";
			std::cin >> format;
			if(SortProduct(field.GetBegin(), format.GetBegin(), productHead)) {
				std::cout << "Product successfuly sort" << std::endl;
			}
		}
		else if (action == "6") {
			Print(productHead);
		}
	} 
	else if(object == "4") {
		this->RenderMain();
		std::cin >> action;
		system("cls");
		if (action == "1") {
			saleHead = AddSale(saleHead);
			std::cout << "Sale successfully added" << std::endl;
		}
		else if (action == "2") {
			Print(saleHead);
			int itemDelete = 0;
			try {
				std::cout << "Input delete number: ";
				std::cin >> itemDelete;
				if (itemDelete <= 0 || itemDelete > GetListSize(saleHead)) {
					throw Error("Sale with this number does not exist");
				}
				saleHead = Delete(itemDelete, saleHead);
				std::cout << "Sale successfully delete" << std::endl;
			}
			catch (Error& error) {
				error.Print();
			}
		}
		else if (action == "3") {
			Print(saleHead);
			int itemEdit = 0;
			while (itemEdit <= 0 || itemEdit > GetListSize(saleHead)) {
				std::cout << "Input edit number: ";
				std::cin >> itemEdit;
			}
			if (EditSale(itemEdit, saleHead)) {
				std::cout << "Sale successfully edited" << std::endl;
			}
		}
		else if (action == "4") {
			String firstDate{}, lastDate{};
			while (firstDate.GetLength() == 1) {
				std::cout << "Input first date: ";
				std::cin >> firstDate;
			}
			while (lastDate.GetLength() == 1) {
				std::cout << "Input last date: ";
				std::cin >> lastDate;
			}
			saleHead = FilterSale(saleHead, firstDate.GetBegin(), lastDate.GetBegin());
			if (saleHead) {
				std::cout << "Sale successfully filtered" << std::endl;
				Print(saleHead);
			}
			else {
				std::cout << "No one item was not selected" << std::endl;
			}
		}
		else if (action == "5") {
			String field{}, format{};
			std::cout << "ID\nCreate date\nTotal price\nInput field name sort: ";
			std::cin >> field;
			std::cout << "ASC\nDESC\nInput sort order: ";
			std::cin >> format;
			if (SortSale(field.GetBegin(), format.GetBegin(), saleHead)) {
				std::cout << "Product successfuly sort" << std::endl;
			}
		}
		else if (action == "6") {
			Print(saleHead);
		}
	}
	return 0;
}

void Editor::RenderMain() const
{
	std::cout << "Editor menu" << std::endl;
	std::cout << "1. Add" << std::endl;
	std::cout << "2. Delete" << std::endl;
	std::cout << "3. Edit" << std::endl;
	std::cout << "4. Filter" << std::endl;
	std::cout << "5. Sort" << std::endl;
	std::cout << "6. Print" << std::endl;
}

int EditorNav() {
	Editor editor{};
	int screen = 0;
	editor.Start(screen);
	return 0;
}

void Editor::PrintItemName() const
{
	std::cout << "1. Client" << std::endl;
	std::cout << "2. Employee" << std::endl;
	std::cout << "3. Product" << std::endl;
	std::cout << "4. Sale" << std::endl;
}