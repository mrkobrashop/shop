#include <iostream>
#include "String/String.h"
#include "Menu/Menu.h"
#include "User/User.h"
#include "Global/Global.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	// �������� ���������� ���������
	State state{};
	state.Upload();

	// ����
	ItemMenu* Items = new ItemMenu[5]{};
	Items[0].Set(String("Authorization"), &Authorization);
	Items[1].Set(String("Editor"), &EditorNav);
	Items[2].Set(String("Shop"), &ShopNav);
	Items[3].Set(String("Report"), &ReportNav);
	Items[4].Set(String("Save state"), &StateNav);
	Menu* MainMenu = new Menu("Main menu", 5, Items);
	MainMenu->ProcessMenu();
 	return 0;
}