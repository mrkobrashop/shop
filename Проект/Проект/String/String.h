#pragma once
#include <iostream>

class String {
	class StrIterator {
		friend class String;
	public:
		StrIterator(const StrIterator& item);
		bool operator== (const StrIterator& item) const;
		bool operator!= (const StrIterator& item) const;
		StrIterator& operator++ ();
		char& operator* ();
	private:
		char* _symbol;
		StrIterator(char* str) : _symbol(str) {}
	};
	typedef StrIterator iterator;
	typedef StrIterator constIterator;
public:
	String();
	String(const char* str);
	String(const char* str_1, const char* str_2);
	String(const String& str);
	~String();
	void AddSymbol(const char symbol);
	void AddString(const char* str);
	String* Splite(const char symbol);
	friend std::ostream& operator<< (std::ostream& out, const String& str);
	friend std::istream& operator>> (std::istream& in, String& str);
	friend bool operator== (const String& str_1 , const String& str_2);
	friend bool operator== (const String& str_1, const char* str_2);
	friend bool operator!= (const String& str_1, const String& str_2);
	friend bool operator!= (const String& str_1, const char* str_2);
	friend bool operator>= (const String& str_1, const String& str_2);
	friend bool operator<= (const String& str_1, const String& str_2);
	friend bool operator> (const String& str_1, const String& str_2);
	friend bool operator< (const String& str_1, const String& str_2);
	String& operator+ (const char* str);
	char& operator[] (const int index);
	unsigned int GetLength();
	char* GetBegin();
	char* GetEnd();
	iterator end();
	iterator begin();
	constIterator begin() const;
	constIterator end() const;
private:
	unsigned int _len;
	char* _string;
	char* _end;
};
