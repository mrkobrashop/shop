#include "String.h"
String::String()
{
	_len = 0;
	_string = new char[++_len]{ '\0' };
	_end = _string + 1;
}

String::String(const char* str) : String()
{
	AddString(str);
}

String::String(const char* str_1, const char* str_2) : String(str_1)
{
	AddString(str_2);
}

String::String(const String& str) : String() {
	_len = str._len;
	_string = str._string;
	_end = str._end;
}

String::~String()
{
	/*
	if (_string != nullptr) {
		delete[] _string;
	}
	if (_end != nullptr) {
		delete _end;
	} */
}

void String::AddString(const char* str) 
{
	for (int i = 0; i < strlen(str); i++)
	{
		AddSymbol(str[i]);
	}
}

void String::AddSymbol(const char symbol)
{
	char* temp = _string;
	_string = new char[++_len]{};
	for (int i = 0; i < (_len - 2); ++i)
	{
		_string[i] = temp[i];
	}
	_string[_len - 2] = symbol;
	_string[_len - 1] = '\0';
	_end = _string + _len;
	delete[] temp;
}

std::ostream& operator<< (std::ostream& out, const String& str)
{
	out << str._string;
	return out;
}

std::istream& operator>> (std::istream& in, String& str)
{
	char symbol;
	str._len = 0;
	str._string = new char[++str._len]{ '\0' };
	str._end = str._string + 1;
	while (in.get(symbol) && symbol != '\n') {
		str.AddSymbol(symbol);
	}
	return in;
}

unsigned int String::GetLength()
{
	return _len;
}

char* String::GetBegin()
{
	return _string;
}

char* String::GetEnd()
{
	return _end;
}

String::StrIterator::StrIterator(const StrIterator& item) : _symbol(item._symbol) {}

bool String::StrIterator::operator== (const StrIterator& item) const
{
	return _symbol == item._symbol;
}

bool String::StrIterator::operator!= (const StrIterator& item) const
{
	return _symbol != item._symbol;
}

String::StrIterator& String::StrIterator::operator++ ()
{
	++_symbol;
	return *this;
}

char& String::StrIterator::operator* ()
{
	return *_symbol;
}

String::iterator String::begin()
{
	return iterator(_string);
}

String::iterator String::end()
{
	return iterator(_end);
}

String::constIterator String::begin() const
{
	return constIterator(_string);
}

String::constIterator String::end() const
{
	return constIterator(_end);
}

bool operator==(const String& str_1, const String& str_2) 
{
	bool result = false;
	int compare = strcmp(str_1._string, str_2._string);
	if (compare == 0) {
		result = true;
	}
	return result;
}

bool operator==(const String& str_1, const char* str_2)
{
	bool result = false;
	int compare = strcmp(str_1._string, str_2);
	if (compare == 0) {
		result = true;
	}
	return result;
}

bool operator!=(const String& str_1, const String& str_2)
{
	bool result = true;
	int compare = strcmp(str_1._string, str_2._string);
	if (compare == 0) {
		result = false;
	}
	return result;
}

bool operator!=(const String& str_1, const char* str_2)
{
	bool result = true;
	int compare = strcmp(str_1._string, str_2);
	if (compare == 0) {
		result = false;
	}
	return result;
}

bool operator>= (const String& str_1, const String& str_2)
{
	bool result = false;
	int compare = strcmp(str_1._string, str_2._string);
	if (compare >= 0) {
		result = true;
	}
	return result;
}

bool operator<= (const String& str_1, const String& str_2)
{
	bool result = false;
	int compare = strcmp(str_1._string, str_2._string);
	if (compare <= 0) {
		result = true;
	}
	return result;
}

bool operator> (const String& str_1, const String& str_2)
{
	bool result = false;
	int compare = strcmp(str_1._string, str_2._string);
	if (compare > 0) {
		result = true;
	}
	return result;
}

bool operator< (const String& str_1, const String& str_2)
{
	bool result = false;
	int compare = strcmp(str_1._string, str_2._string);
	if (compare < 0) {
		result = true;
	}
	return result;
}

char& String::operator[] (const int index)
{
	return _string[index];
}

String& String::operator+ (const char* str)
{
	AddString(str);
	return *this;
}

String* String::Splite(const char symbol)
{
	int count = 0, splite = 0;
	String* result = nullptr;
	for (int i = 0; i < _len; i++)
	{
		if (_string[i] == symbol) {
			count++;
		}
	}
	if (count != 0) {
		result = new String[count + 1]{};
		for (int i = 0; i < _len; i++)
		{
			if (_string[i] == symbol) {
				splite++;
			}
			else if(_string[i] != '\0') {
				result[splite].AddSymbol(_string[i]);
			}
		}
	}
	return result;
}